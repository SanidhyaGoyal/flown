# Backend Springboot

## Installation

**1. Clone the repository**
```bash
git clone https://SanidhyaGoyal@bitbucket.org/SanidhyaGoyal/flown.git
```
**2. Configure PostgreSQL**

First, create a database named `postgres_demo`. Then, open `src/main/resources/application.properties` file and change the spring datasource username and password as per your PostgreSQL installation.

**3. Run the app**

Run the main method of -

`src/main/java/com/example/crud/CrudApplication.java`

**4. Insertion of data into table**

Copy the request body from `backend/post_request.json` file and hit the POST url http://localhost:8080/users to insert users data in table.