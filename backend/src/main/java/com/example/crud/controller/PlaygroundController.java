package com.example.crud.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/playground")
public class PlaygroundController {

    @GetMapping("/market")
    public String market(){
        return  "Fusce euismod ligula vitae commodo feugiat. Aliquam sagittis, arcu ut lacinia ultrices, lectus elit varius ligula, ac maximus lorem erat eget justo. Integer nec pretium lorem. Vestibulum ac est tellus. Nullam vehicula et nibh in consectetur. Aliquam vel odio lorem. Nullam sed mattis tortor, non volutpat est. Fusce tristique justo mattis ornare efficitur. Curabitur vel sem nec tortor sagittis commodo nec egestas lacus.";
    }

    @GetMapping("/flights")
    public String flights(){
        return  "Maecenas malesuada libero quis vehicula feugiat. Aliquam vitae nisi a leo rutrum facilisis cursus at orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In hac habitasse platea dictumst. Vivamus id pellentesque felis, et tincidunt lectus. Vestibulum viverra dui at mauris porta molestie. Integer a consectetur velit. Fusce neque massa, accumsan non justo a, scelerisque rutrum mauris. Praesent vitae sagittis tortor, quis feugiat nulla. Nulla luctus lectus ullamcorper mi ullamcorper, sed pulvinar orci bibendum. Nam bibendum rutrum nulla, vel varius quam gravida in.";
    }

    @GetMapping("/prediction")
    public String prediction(){
        return  "In eu orci a ante volutpat iaculis nec ac lectus. Praesent semper odio nec gravida malesuada. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque vel consequat lacus. In lobortis justo eget magna maximus, quis consectetur tortor iaculis. Suspendisse in eros convallis ex ornare dignissim. Nam at congue tellus, sed tempus leo. Donec lobortis consectetur enim eget placerat. Nullam blandit sed purus et malesuada. Pellentesque et molestie odio.";
    }

}