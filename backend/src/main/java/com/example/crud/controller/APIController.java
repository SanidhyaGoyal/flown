package com.example.crud.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class APIController {

    @GetMapping("/user")
    public String user(){
        return  "Eleifend donec pretium vulputate sapien nec sagittis aliquam. Facilisis mauris sit amet massa vitae tortor condimentum lacinia quis. Pellentesque diam volutpat commodo sed egestas. Nisl vel pretium lectus quam. Ultrices tincidunt arcu non sodales neque. Neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Tincidunt id aliquet risus feugiat in ante metus. Id aliquet lectus proin nibh nisl condimentum id. Lectus urna duis convallis convallis. Velit euismod in pellentesque massa. Nec nam aliquam sem et tortor consequat id. Eget egestas purus viverra accumsan in nisl nisi scelerisque eu. Enim tortor at auctor urna. Sollicitudin ac orci phasellus egestas tellus rutrum tellus. Et ultrices neque ornare aenean euismod elementum nisi.";
    }

    @GetMapping("/quotes")
    public String quotes(){
        return  "Imperdiet nulla malesuada pellentesque elit eget gravida cum sociis natoque. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam etiam. Sollicitudin ac orci phasellus egestas. Sed id semper risus in. Velit sed ullamcorper morbi tincidunt ornare massa eget egestas purus. Aliquet nibh praesent tristique magna. Sed enim ut sem viverra aliquet eget. Tincidunt ornare massa eget egestas purus viverra accumsan in nisl. Pharetra vel turpis nunc eget lorem dolor. Eu augue ut lectus arcu bibendum at varius vel. Vitae purus faucibus ornare suspendisse sed. Scelerisque mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus et. Pellentesque habitant morbi tristique senectus et. Etiam tempor orci eu lobortis. Eget egestas purus viverra accumsan in. Sed euismod nisi porta lorem mollis aliquam. Tristique senectus et netus et malesuada fames. Rutrum tellus pellentesque eu tincidunt. Purus non enim praesent elementum facilisis. Non pulvinar neque laoreet suspendisse interdum.";
    }

    @GetMapping("/track")
    public String track(){
        return  "Adipiscing vitae proin sagittis nisl rhoncus mattis rhoncus urna neque. Placerat vestibulum lectus mauris ultrices eros in cursus. In hendrerit gravida rutrum quisque. Lacinia quis vel eros donec ac odio tempor. Amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus. Integer feugiat scelerisque varius morbi enim nunc. Viverra suspendisse potenti nullam ac tortor vitae purus faucibus. Sit amet porttitor eget dolor. Commodo viverra maecenas accumsan lacus vel facilisis volutpat. Convallis tellus id interdum velit laoreet id donec ultrices. Gravida rutrum quisque non tellus orci ac. Est ante in nibh mauris cursus mattis molestie.";
    }

    @GetMapping("/airline")
    public String airline(){
        return  "Quam quisque id diam vel quam elementum pulvinar etiam. Augue interdum velit euismod in pellentesque massa placerat duis. Integer quis auctor elit sed vulputate mi sit amet. Lobortis feugiat vivamus at augue. In fermentum et sollicitudin ac orci phasellus egestas. Lacus sed viverra tellus in hac habitasse platea dictumst. Id venenatis a condimentum vitae. Quis commodo odio aenean sed adipiscing diam donec adipiscing tristique. Risus pretium quam vulputate dignissim suspendisse. Euismod elementum nisi quis eleifend quam adipiscing vitae. Dis parturient montes nascetur ridiculus mus mauris vitae ultricies. Hac habitasse platea dictumst quisque sagittis. Massa eget egestas purus viverra accumsan in nisl nisi. Urna cursus eget nunc scelerisque viverra mauris in aliquam. At tellus at urna condimentum mattis. Vestibulum lectus mauris ultrices eros in cursus turpis.";
    }

    @GetMapping("/policy")
    public String policy(){
        return  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ipsum faucibus vitae aliquet nec ullamcorper sit. Blandit turpis cursus in hac habitasse. Ut porttitor leo a diam sollicitudin tempor id. Vitae justo eget magna fermentum iaculis. Rhoncus dolor purus non enim praesent elementum facilisis. Eu lobortis elementum nibh tellus molestie nunc non blandit. Mattis ullamcorper velit sed ullamcorper. Tincidunt eget nullam non nisi est sit. Nam at lectus urna duis convallis convallis tellus id. Tincidunt dui ut ornare lectus sit amet. Leo a diam sollicitudin tempor id eu nisl nunc mi. Consectetur a erat nam at. Id velit ut tortor pretium viverra suspendisse potenti nullam. Vestibulum sed arcu non odio euismod. A erat nam at lectus urna. Nec tincidunt praesent semper feugiat nibh sed pulvinar. Id venenatis a condimentum vitae sapien pellentesque habitant morbi tristique.";
    }

    @GetMapping("/statistics")
    public String statistics(){
        return  "Pellentesque pulvinar pellentesque habitant morbi. Pharetra diam sit amet nisl suscipit adipiscing bibendum est. At elementum eu facilisis sed. Montes nascetur ridiculus mus mauris vitae ultricies leo integer malesuada. Nullam ac tortor vitae purus faucibus ornare. Eget arcu dictum varius duis at consectetur lorem donec. Tellus in metus vulputate eu scelerisque felis imperdiet proin. Consectetur adipiscing elit pellentesque habitant morbi. Arcu non odio euismod lacinia at. Tempor orci dapibus ultrices in. Suspendisse potenti nullam ac tortor vitae purus faucibus ornare. Neque gravida in fermentum et sollicitudin. Eget aliquet nibh praesent tristique magna sit amet. Vulputate dignissim suspendisse in est ante in. Turpis egestas maecenas pharetra convallis. Dignissim diam quis enim lobortis scelerisque fermentum. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at. Pellentesque dignissim enim sit amet venenatis urna cursus eget nunc. Ornare lectus sit amet est placerat in egestas.";
    }

}