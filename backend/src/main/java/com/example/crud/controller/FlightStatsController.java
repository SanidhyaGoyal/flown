package com.example.crud.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping()
public class FlightStatsController {

    @GetMapping("/flight_stats")
    public String flightStats(){
        return  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi aliquam massa nisl, sed dictum diam feugiat non. Cras fermentum non augue sed eleifend. Nullam eu gravida ante. Aliquam commodo erat sed tincidunt rutrum. Proin iaculis tincidunt massa, ac dapibus dolor pulvinar sed. Curabitur vestibulum lectus mauris, et convallis tortor semper id. Cras ultrices vehicula turpis, vel rhoncus turpis luctus non. Nulla dolor elit, sagittis ut ipsum ut, tincidunt ultrices mi. Nulla scelerisque lorem ante, a semper augue volutpat ac. In hac habitasse platea dictumst. Suspendisse accumsan sed felis ac feugiat. Donec vulputate tempor magna. In eget ultrices massa, non vestibulum dui. Suspendisse vulputate risus a augue molestie, eget imperdiet lacus vehicula. Quisque sit amet tortor eu turpis dapibus venenatis.";
    }

}