package com.example.crud.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping()
public class NotificationsController {

    @GetMapping("/notifications")
    public String notifications(){
        return  "Eleifend donec pretium vulputate sapien nec sagittis aliquam. Facilisis mauris sit amet massa vitae tortor condimentum lacinia quis. Pellentesque diam volutpat commodo sed egestas. Nisl vel pretium lectus quam. Ultrices tincidunt arcu non sodales neque. Neque laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Tincidunt id aliquet risus feugiat in ante metus. Id aliquet lectus proin nibh nisl condimentum id. Lectus urna duis convallis convallis. Velit euismod in pellentesque massa. Nec nam aliquam sem et tortor consequat id. Eget egestas purus viverra accumsan in nisl nisi scelerisque eu. Enim tortor at auctor urna. Sollicitudin ac orci phasellus egestas tellus rutrum tellus. Et ultrices neque ornare aenean euismod elementum nisi.";
    }

}