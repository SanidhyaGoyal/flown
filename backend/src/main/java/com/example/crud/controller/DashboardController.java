package com.example.crud.controller;

import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping()
public class DashboardController {

    @GetMapping("/downloads")
    public String downloads(){
        return  "Venenatis a condimentum vitae sapien. Venenatis urna cursus eget nunc scelerisque viverra mauris in. Elit duis tristique sollicitudin nibh sit amet commodo nulla facilisi. Nulla facilisi cras fermentum odio eu feugiat. Nisl rhoncus mattis rhoncus urna neque viverra justo nec. Pretium lectus quam id leo in vitae turpis. Vitae elementum curabitur vitae nunc sed velit dignissim sodales ut. Orci sagittis eu volutpat odio facilisis mauris sit amet massa. Platea dictumst quisque sagittis purus sit amet. Ac auctor augue mauris augue neque gravida in fermentum. Sagittis eu volutpat odio facilisis mauris sit amet massa. Turpis egestas maecenas pharetra convallis. Odio morbi quis commodo odio aenean sed adipiscing diam. Nec dui nunc mattis enim ut tellus.";
    }

    @GetMapping("/audit")
    public String audits(){
        return  "Ullamcorper malesuada proin libero nunc consequat. Eleifend donec pretium vulputate sapien nec sagittis. Dolor sed viverra ipsum nunc aliquet bibendum enim facilisis. Lacus vestibulum sed arcu non odio. Etiam non quam lacus suspendisse faucibus interdum posuere lorem. Tristique et egestas quis ipsum suspendisse ultrices gravida. Nam at lectus urna duis convallis. Risus at ultrices mi tempus imperdiet. Ipsum faucibus vitae aliquet nec ullamcorper. Ac turpis egestas sed tempus urna et pharetra.";

    }


}