package com.example.crud.controller;

import com.example.crud.model.Users;
import com.example.crud.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping()
public class UsersController {

    @Autowired
    private UsersRepository userRepository;

    @GetMapping("/users")
    public List<Users> getUsers(){
        return  userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public Optional<Users> getUsersById(@PathVariable(value = "id") int id){
        return  userRepository.findById(id);
    }

    @PostMapping("/users")
    public List<Users> insertUsers(@RequestBody List<Users> list){
        return userRepository.saveAll(list);
    }

}