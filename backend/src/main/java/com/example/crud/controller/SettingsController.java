package com.example.crud.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/settings")
public class SettingsController {

    @GetMapping("/security")
    public String security(){
        return  "Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend. Mauris commodo quis imperdiet massa tincidunt nunc pulvinar. Est pellentesque elit ullamcorper dignissim. Enim lobortis scelerisque fermentum dui faucibus in ornare. Pharetra pharetra massa massa ultricies mi quis. Ultrices eros in cursus turpis massa tincidunt dui ut ornare. Elementum eu facilisis sed odio morbi quis commodo. Amet justo donec enim diam vulputate ut pharetra sit amet. Integer feugiat scelerisque varius morbi enim nunc faucibus a. Vel fringilla est ullamcorper eget nulla facilisi. Tristique senectus et netus et. Interdum varius sit amet mattis vulputate enim. Venenatis tellus in metus vulputate eu scelerisque. Imperdiet dui accumsan sit amet nulla facilisi. Mattis nunc sed blandit libero volutpat sed cras. Quam nulla porttitor massa id neque aliquam vestibulum morbi blandit. Nisi vitae suscipit tellus mauris a diam maecenas sed. Pellentesque massa placerat duis ultricies. Facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris.";
    }

    @GetMapping("/notifications")
    public String notifications(){
        return  "Adipiscing enim eu turpis egestas pretium aenean pharetra. Faucibus ornare suspendisse sed nisi lacus sed. Cursus euismod quis viverra nibh cras. Ante metus dictum at tempor commodo ullamcorper. Nunc scelerisque viverra mauris in aliquam sem. Lorem ipsum dolor sit amet consectetur adipiscing elit pellentesque habitant. Imperdiet proin fermentum leo vel orci porta non pulvinar. Morbi tincidunt ornare massa eget egestas purus viverra accumsan in. Amet risus nullam eget felis eget nunc lobortis. Montes nascetur ridiculus mus mauris. Non odio euismod lacinia at quis risus sed vulputate odio. Eu non diam phasellus vestibulum.";
    }

    @GetMapping("/sandbox")
    public String sandbox(){
        return  "Donec enim diam vulputate ut. At elementum eu facilisis sed odio morbi quis. Convallis convallis tellus id interdum velit laoreet. Sed libero enim sed faucibus turpis in eu mi bibendum. Consectetur adipiscing elit duis tristique sollicitudin nibh sit. Risus commodo viverra maecenas accumsan lacus vel. Scelerisque purus semper eget duis at tellus at urna condimentum. Sit amet tellus cras adipiscing enim. Donec enim diam vulputate ut pharetra sit amet. Risus commodo viverra maecenas accumsan lacus vel. Sed enim ut sem viverra aliquet eget sit amet. In nisl nisi scelerisque eu ultrices vitae. Sed sed risus pretium quam vulputate dignissim suspendisse in est. Mi sit amet mauris commodo quis imperdiet massa. Etiam non quam lacus suspendisse faucibus interdum posuere. Hac habitasse platea dictumst quisque sagittis purus sit amet volutpat. Faucibus nisl tincidunt eget nullam non nisi.";
    }

    @GetMapping("/disable_coverage")
    public String disableCoverage(){
        return  "Massa tincidunt dui ut ornare lectus. Nunc non blandit massa enim. Magna ac placerat vestibulum lectus mauris ultrices eros in cursus. Risus sed vulputate odio ut. Enim nunc faucibus a pellentesque. Porttitor eget dolor morbi non arcu. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et. Morbi leo urna molestie at elementum eu facilisis sed odio. Donec et odio pellentesque diam volutpat commodo. Turpis egestas integer eget aliquet nibh praesent tristique magna sit. Sed ullamcorper morbi tincidunt ornare massa eget egestas. Turpis tincidunt id aliquet risus. Mauris ultrices eros in cursus.";
    }
}