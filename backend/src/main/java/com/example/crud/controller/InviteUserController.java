package com.example.crud.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping()
public class InviteUserController {

    @GetMapping("/invite")
    public String inviteUser(){
        return  "Fusce euismod ligula vitae commodo feugiat. Aliquam sagittis, arcu ut lacinia ultrices, lectus elit varius ligula, ac maximus lorem erat eget justo. Integer nec pretium lorem. Vestibulum ac est tellus. Nullam vehicula et nibh in consectetur. Aliquam vel odio lorem. Nullam sed mattis tortor, non volutpat est. Fusce tristique justo mattis ornare efficitur. Curabitur vel sem nec tortor sagittis commodo nec egestas lacus.";
    }

}

