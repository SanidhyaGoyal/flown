import React from 'react';

const Toaster = React.lazy(() => import('./views/notifications/toaster/Toaster'));
const Tables = React.lazy(() => import('./views/base/tables/Tables'));

const Breadcrumbs = React.lazy(() => import('./views/base/breadcrumbs/Breadcrumbs'));
const Cards = React.lazy(() => import('./views/base/cards/Cards'));
const Carousels = React.lazy(() => import('./views/base/carousels/Carousels'));
const Collapses = React.lazy(() => import('./views/base/collapses/Collapses'));
const BasicForms = React.lazy(() => import('./views/base/forms/BasicForms'));

const Jumbotrons = React.lazy(() => import('./views/base/jumbotrons/Jumbotrons'));
const ListGroups = React.lazy(() => import('./views/base/list-groups/ListGroups'));
const Navbars = React.lazy(() => import('./views/base/navbars/Navbars'));
const Navs = React.lazy(() => import('./views/base/navs/Navs'));
const Paginations = React.lazy(() => import('./views/base/paginations/Pagnations'));
const Popovers = React.lazy(() => import('./views/base/popovers/Popovers'));
const ProgressBar = React.lazy(() => import('./views/base/progress-bar/ProgressBar'));
const Switches = React.lazy(() => import('./views/base/switches/Switches'));

const Tabs = React.lazy(() => import('./views/base/tabs/Tabs'));
const Tooltips = React.lazy(() => import('./views/base/tooltips/Tooltips'));
const BrandButtons = React.lazy(() => import('./views/buttons/brand-buttons/BrandButtons'));
const ButtonDropdowns = React.lazy(() => import('./views/buttons/button-dropdowns/ButtonDropdowns'));
const ButtonGroups = React.lazy(() => import('./views/buttons/button-groups/ButtonGroups'));
const Buttons = React.lazy(() => import('./views/buttons/buttons/Buttons'));
const Charts = React.lazy(() => import('./views/charts/Charts'));
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const CoreUIIcons = React.lazy(() => import('./views/icons/coreui-icons/CoreUIIcons'));
const Flags = React.lazy(() => import('./views/icons/flags/Flags'));
const Brands = React.lazy(() => import('./views/icons/brands/Brands'));
// const Alerts = React.lazy(() => import('./views/notifications/alerts/Alerts'));
// const Badges = React.lazy(() => import('./views/notifications/badges/Badges'));
// const Modals = React.lazy(() => import('./views/notifications/modals/Modals'));
//const Colors = React.lazy(() => import('./views/theme/colors/Colors'));
//const Typography = React.lazy(() => import('./views/theme/typography/Typography'));
// const Widgets = React.lazy(() => import('./views/widgets/Widgets'));
const Users = React.lazy(() => import('./views/users/Users'));
const User = React.lazy(() => import('./views/users/User'));

const Monitoring = React.lazy(() => import('./views/monitoring/Monitoring'));
const FlightStats = React.lazy(() => import('./views/flightStats/FlightStats'));
const Market = React.lazy(() => import('./views/playground/Market'));
const Flights = React.lazy(() => import('./views/playground/Flights'));
const Prediction = React.lazy(() => import('./views/playground/Prediction'));
const UserAPI = React.lazy(() => import('./views/api/User'));
const Quotes = React.lazy(() => import('./views/api/Quotes'));
const Track = React.lazy(() => import('./views/api/Track'));
const Airline = React.lazy(() => import('./views/api/Airline'));
const Policy = React.lazy(() => import('./views/api/Policy'));
const Statistics = React.lazy(() => import('./views/api/Statistics'));
const Notifications = React.lazy(() => import('./views/notifications/Notifications'));

const Sandbox = React.lazy(() => import('./views/settings/Sandbox'));
const Security = React.lazy(() => import('./views/settings/Security'));
const Notification_Settings = React.lazy(() => import('./views/settings/Notifications'));
const DisableCoverage = React.lazy(() => import('./views/settings/DisableCoverage'));
const InviteUsers = React.lazy(() => import('./views/users/InviteUsers'));
const Downloads = React.lazy(() => import('./views/downloads/Downloads'));
const Audits = React.lazy(() => import('./views/audits/Audits'));


const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  //{ path: '/theme', name: 'Theme', component: Colors, exact: true },
  //{ path: '/theme/colors', name: 'Colors', component: Colors },
  //{ path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', name: 'Base', component: Cards, exact: true },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/forms', name: 'Forms', component: BasicForms },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', name: 'Buttons', component: Buttons, exact: true },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/sales_ravenue', name: 'Sales Ravenue', component: Charts },
  { path: '/monitoring', name: 'Performance Monitoring', component: Monitoring },
  { path: '/flight_stats', name: 'Flight Stats', component: FlightStats },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/brands', name: 'Brands', component: Brands },
  { path: '/playground', name: 'Playground', component: Market, exact: true },
  { path: '/playground/market', name: 'Market', component: Market },
  { path: '/playground/flights', name: 'Flights', component: Flights },
  { path: '/playground/prediction', name: 'Prediction', component: Prediction },
  { path: '/notifications/toaster', name: 'Toaster', component: Toaster },
  { path: '/api', name: 'API', component: UserAPI, exact: true },
  { path: '/api/user', name: 'User', component: UserAPI },
  { path: '/api/quotes', name: 'Quotes', component: Quotes },
  { path: '/api/track', name: 'Track', component: Track },
  { path: '/api/airline', name: 'Airline', component: Airline },
  { path: '/api/policy', name: 'Policy', component: Policy },
  { path: '/api/statistics', name: 'Statistics', component: Statistics },
  { path: '/notifications', name: 'Notifications', component: Notifications },
  { path: '/notifications/toaster', name: 'Toaster', component: Toaster },
  { path: '/settings', name: 'Settings', component: Security, exact: true },
  { path: '/settings/security', name: 'Security', component: Security },
  { path: '/settings/notifications', name: 'Notifications', component: Notification_Settings },
  { path: '/settings/sandbox', name: 'Sandbox', component: Sandbox },
  { path: '/settings/disable_coverage', name: 'Disable Coverage', component: DisableCoverage },
  { path: '/invite', name: 'Invite Users', component: InviteUsers },
  
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  { path: '/downloads', exact: true,  name: 'Downloads', component: Downloads },
  { path: '/audits', exact: true,  name: 'Audits', component: Audits },
];

export default routes;
