export default [
  {
    _tag: 'CSidebarNavItem',
    name: 'Airline Dashboard',
    to: '/dashboard',
    icon: 'cil-speedometer',
  },

  {
    _tag: 'CSidebarNavTitle',
    _children: ['Components']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Sales & Revenue',
    to: '/sales_ravenue',
    icon: 'cil-dollar'
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Performance Monitoring',
    to: '/monitoring',
    icon: 'cil-chart-pie',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Flight Stats',
    to: '/flight_stats',
    icon: 'cil-flight-takeoff'
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Playground',
    route: '/icons',
    icon: 'cil-calculator',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Market',
        to: '/playground/market',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Flights',
        to: '/playground/flights',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Prediction',
        to: '/playground/prediction',
      }
    ]
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'API',
    route: '/api',
    icon: 'cil-code',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'User',
        to: '/api/user',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Quotes',
        to: '/api/quotes',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'RealTime Track',
        to: '/api/track',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Airlines',
        to: '/api/airline',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Policy',
        to: '/api/policy',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Statistics',
        to: '/api/statistics',
      }
    ]
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Notifications',
    to: '/notifications',
    icon: 'cil-bell',
    badge: {
      color: 'info',
      text: 'NEW',
    },
  },
  {
    _tag: 'CSidebarNavDivider'
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Extras'],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Settings',
    route: '/pages',
    icon: 'cil-settings',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Security',
        to: '/settings/security',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Notifications',
        to: '/settings/notifications',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Sandbox',
        to: '/settings/sandbox',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Disable Coverage',
        to: '/settings/disable_coverage',
      }
    ]
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Invite Users',
    to: '/invite',
    icon: 'cil-user-plus',
    badge: {
      color: 'info',
      text: 'NEW',
    },
  },
  {
    _tag: 'CSidebarNavDivider',
    className: 'm-2'
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Labels']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Label danger',
    to: '',
    icon: {
      name: 'cil-star',
      className: 'text-danger'
    },
    label: true
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Label info',
    to: '',
    icon: {
      name: 'cil-star',
      className: 'text-info'
    },
    label: true
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Label warning',
    to: '',
    icon: {
      name: 'cil-star',
      className: 'text-warning'
    },
    label: true
  },
  {
    _tag: 'CSidebarNavDivider',
    className: 'm-2'
  }
]

