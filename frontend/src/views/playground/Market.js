import React,{useState, useEffect} from 'react'
import axios from 'axios'


const Market = ({match}) => {
  const [data, setData] = useState('')
  //const [title, setTitle] = useState('there')
  const getData = () => {
    let temp_data = data
    axios.get('http://localhost:8080/playground/market')
    .then(res => {
      setData(res.data)
    })
    .catch(e => {
      console.log("error: ",e)
    })
  }

  useEffect(() => {
    getData()
  },[])

  return (
    <div className="container">
    <h1>{'Market'}</h1>
    <p>{data}</p>
    </div>
  )
}

export default Market
