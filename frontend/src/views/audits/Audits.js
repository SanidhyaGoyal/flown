import React,{useState, useEffect} from 'react'
import logo from  '../../assets/icons/logo.svg'
import axios from 'axios'


const Audits = ({match}) => {
  const [data, setData] = useState('')
  //const [title, setTitle] = useState('there')
  const getData = () => {
    let temp_data = data
    axios.get('http://localhost:8080/audit')
    .then(res => {
      setData(res.data)
    })
    .catch(e => {
      console.log("error: ",e)
    })
  }

  useEffect(() => {
    getData()
  },[])
  
  return (
    <div className='container'>
    <figure>
      <img className="img-fluid logo" width="30%" src={logo} alt="GoFlown" />
      <figcaption> Enjoy your flight we take care of Delay </figcaption>
    </figure>
    <h1>{'Audits'}</h1>
    <p>{data}</p>
    </div>
  )
}

export default Audits