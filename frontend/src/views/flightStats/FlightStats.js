import React,{useState, useEffect} from 'react'
import axios from 'axios'

const FlightStats = ({match}) => {
  const [data, setData] = useState('')
  //const [title, setTitle] = useState('there')
  const getData = () => {
    let temp_data = data
    axios.get('http://localhost:8080/flight_stats')
    .then(res => {
      setData(res.data)
    })
    .catch(e => {
      console.log("error: ",e)
    })
  }

  useEffect(() => {
    getData()
  },[])
  return (
    <div className="container">
    <h1>{'Flight Stats'}</h1>
    <p>{data}</p>
    </div>
  )
}

export default FlightStats
